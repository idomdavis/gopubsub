# gopubsub

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/gopubsub/main?style=plastic)](https://bitbucket.org/idomdavis/gopubsub/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/gopubsub?style=plastic)](https://bitbucket.org/idomdavis/gopubsub/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/gopubsub?style=plastic)](https://bitbucket.org/idomdavis/gopubsub/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/gopubsub)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)

Simple Publish and Subscribe implementation using a message broker. Messages are
pushed onto the broker using a channel, and subscribers have their own channels
to read from. Messages are sent asynchronously so a subscriber can't block the
broker, however, this does mean that messages are not guaranteed to be in order
with high volume systems.

## Installation

```shell
go get -u bitbucket.org/idomdavis/gopubsub
```

## Usage

Create a broker of type `T`, where `T` should be replaced by a concrete type.

```
b := gopubsub.Broker[T]{}
```

Start the broker which will return a control channel. This channel will publish
an event of type `struct{}` when the broker has stopped listening.

```
done := b.Listen()

// ... do stuff ...

<-done
// broker has now shut down
```

Messages are published onto the broker using `Publish`. The messages are of type
`T`. Pushing messages at a high volume can cause them to be published out of 
order due to the asynchronous nature of the fan out.

```
v.Publish(msg)
```

To subscribe use `Subscribe` which will return a channel of type `T` to listen
on. You can then unsubscribe using `Unsubscribe`. If the broker is stopped then
the channel `c` will be closed.

```
c := b.Subscribe()

// ... consume and handle messages from c ...

b.Unsubscribe(c)
```

The broker can be stopped by calling `Stop` and it will emit a `struct{}` on
`done` when it has removed all the subscribers.
