package gopubsub

// Broker handles publishing messages passed to it to all the registered
// subscribers. In Synchronous mode the Broker will block until the message has
// been published to all subscribers. Synchronous mode guarantees message order,
// but can impact performance if subscribers are slow to consume messages. When
// not in Synchronous mode the broker dispatches messages in separate goroutines
// which avoids blocking but can cause messages to be published out of order
// under high message throughputs. Adding a Buffer can allow a Synchronous
// broker to retain message order and performance where the message rate is
// bursty. Note that when the buffer for any given subscriber is full then the
// broker can still block.
type Broker[T any] struct {
	Buffer      int
	Synchronous bool

	listening bool

	subscribers map[chan<- T]chan T

	publish     chan T
	subscribe   chan chan T
	unsubscribe chan (<-chan T)

	control chan struct{}
}

// Publish a message of type T onto the broker. The message will be broadcast to
// all current subscribers. Pushing high volumes of messages may result in
// subscribers receiving the messages out of order. Publishing to a broker that
// is not listening will do nothing.
func (b *Broker[T]) Publish(msg T) {
	if !b.listening {
		return
	}

	b.publish <- msg
}

// Subscribe to the broker. Messages published on the broker will be sent on the
// returned channel. If the broker is not listening then the returned channel
// will be closed.
//
// If the Broker is not running in Synchronous mode then messages are not
// guaranteed to be delivered in the order they were published. This is
// especially true under very high message loads.
func (b *Broker[T]) Subscribe() <-chan T {
	c := make(chan T, b.Buffer)

	if !b.listening {
		close(c)
	} else {
		b.subscribe <- c
	}

	return c
}

// Unsubscribe from the broker. No more messages will be published to provided
// channel. Passing in a channel the broker hasn't provided, or unsubscribing
// from a closed broker will do nothing.
func (b *Broker[T]) Unsubscribe(c <-chan T) {
	if !b.listening {
		return
	}

	b.unsubscribe <- c
}

// Stop the broker. There is no guarantee that messages waiting to be broadcast
// will be sent before the broker is stopped. Stopping a stopped broker does
// nothing.
func (b *Broker[T]) Stop() {
	if !b.listening {
		return
	}

	b.listening = false
	b.control <- struct{}{}
}

// Listen for messages, broadcasting them to subscribers. The returned channel
// will receive a message when the broker has been stopped and all subscriber
// channels closed. A stopped broker can be restarted using Listen, however,
// none of its previous state will be remembered.
func (b *Broker[T]) Listen() <-chan struct{} {
	subscribers := map[<-chan T]chan T{}
	done := make(chan struct{})

	b.control = make(chan struct{})
	b.publish = make(chan T)
	b.subscribe = make(chan chan T)
	b.unsubscribe = make(chan (<-chan T))

	go func() {
		for {
			select {
			case msg := <-b.publish:
				for _, c := range subscribers {
					publish(c, msg, b.Synchronous)
				}
			case c := <-b.subscribe:
				subscribers[c] = c
			case c := <-b.unsubscribe:
				if subscriber, ok := subscribers[c]; ok {
					delete(subscribers, c)
					close(subscriber)
				}
			case <-b.control:
				for _, c := range subscribers {
					delete(subscribers, c)
					close(c)
				}

				done <- struct{}{}

				close(b.control)
				close(b.publish)
				close(b.subscribe)
				close(b.unsubscribe)
				close(done)

				return
			}
		}
	}()

	b.listening = true

	return done
}

func publish[T any](c chan T, msg T, synchronous bool) {
	if synchronous {
		send(c, msg)
	} else {
		go send(c, msg)
	}
}

func send[T any](c chan T, msg T) {
	defer func() { _ = recover() }()
	c <- msg
}
