package gopubsub_test

import (
	"testing"

	"bitbucket.org/idomdavis/gopubsub"
)

func TestBroker_Listen(t *testing.T) {
	t.Run("A broker will publish messages when listening", func(t *testing.T) {
		const msg = "Hello"

		b := gopubsub.Broker[string]{}

		done := b.Listen()
		c := b.Subscribe()

		b.Publish(msg)

		if m := <-c; m != msg {
			t.Errorf("Unexpected message: %q", m)
		}

		b.Stop()

		<-done
	})
}

func TestBroker_Unsubscribe(t *testing.T) {
	t.Run("Unsubscribing will stop messages being sent", func(t *testing.T) {
		b := gopubsub.Broker[string]{}

		done := b.Listen()
		c := b.Subscribe()

		b.Unsubscribe(c)
		b.Publish("discarded")

		for range c {
			t.Error("Should not receive any messages")
		}

		b.Stop()

		<-done
	})

	t.Run("Unsubscribing on a stopped broker does nothing", func(t *testing.T) {
		b := gopubsub.Broker[string]{}

		done := b.Listen()
		c := b.Subscribe()

		b.Stop()

		<-done

		b.Unsubscribe(c)
	})

	t.Run("Unsubscribing a random channel does nothing", func(t *testing.T) {
		b := gopubsub.Broker[string]{}

		done := b.Listen()

		c := make(chan string)

		b.Unsubscribe(c)
		b.Stop()

		<-done
	})
}

func TestBroker_Stop(t *testing.T) {
	t.Run("Stopping a stopped broker does nothing", func(t *testing.T) {
		b := gopubsub.Broker[string]{}
		b.Stop()
	})
}

func TestBroker_Publish(t *testing.T) {
	t.Run("Publish does nothing if the broker is not listening", func(t *testing.T) {
		b := gopubsub.Broker[string]{}
		b.Publish("discarded")
	})
}

func TestBroker_Subscribe(t *testing.T) {
	t.Run("Subscribe channels are closed if broker is not listening", func(t *testing.T) {
		b := gopubsub.Broker[string]{}
		c := b.Subscribe()
		<-c
	})
}
