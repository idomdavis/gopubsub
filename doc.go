// Package gopubsub provides point-to-point publish and subscribe semantics
// using channels via a broker. The gopubsub Broker type provides asynchronous
// publishing of messages to zero or more subscribers.
package gopubsub
