package gopubsub_test

import (
	"fmt"

	"bitbucket.org/idomdavis/gopubsub"
)

func ExampleBroker_Listen() {
	const max = 10

	b := gopubsub.Broker[int]{Synchronous: true, Buffer: max}
	done := b.Listen()
	c := b.Subscribe()

	// Run a subscriber in a goroutine. The subscriber will stop the broker
	// when it reads a value of `max`.
	go func() {
		for m := range c {
			fmt.Print(m, ", ")

			if m >= max {
				b.Unsubscribe(c)
			}
		}

		b.Stop()
	}()

	// Publish a set of integers.
	go func() {
		for i := 0; i <= 10; i++ {
			b.Publish(i)
		}
	}()

	<-done
	fmt.Println("done")

	// Output:
	// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, done
}
